# Nagios-CI-testing

This will be a small project to test CI features and eventually turn into a CI on nagios deployment.

# Goal
The main goal is to forfill the learning goals of the current week of SS3. Also to have a small Nagios deployment script ready and working using moozers premade ansible nagios role. We want a localhost deployment and a targets host group deployment up and running.

## Project structure

This project is to use ansible to deploy a nagios role on a machine,  
where we are able to monitor different hosts.

## What is this?

This repository is used to build a complete image of a virtual machine, with the service Nagios on.
It uses ansible to install all the required services inside of the virtual machine which have to be spun up by your own.

The "Ansible" role is fetched from [HERE](https://github.com/moozer/ansible-role-nagios)
which is a already working nagios role where we only have to insert, which hosts we are going to install the host on.

When the installation is done are we able to connect to a working Nagios webclient in your browser at http://123.456.789.0123/nagios

# How to use?

**Localhost**
To use this repository to install nagios on a local machine, the procedure is automated, through a bash script and can be simple done with the commands below.


    git clone https://gitlab.com/ucl-itt-ss3/nagios-ss3

    cd nagios-ss3

    chmod +x runme.sh

    ./runme.sh && ./runme.sh

**Deploy on current enviroment**
Firstly is to change the inventory files, with your hosts to your spicified enviroment, so ansible can target the hosts.

Inside the path /inventory/test/ssh_config

Add the hosts ip addresses and deploy the public of the ansible host to the devices. **Make sure the target devices has python installed.**

If any new devices is added that aren't present in the ssh_config file. Make sure to update the entry in the inventory file aswell, with the given hostname.

To run the script and install nagioshost on a target machine. go to the path of git and execute the commands below.

        cd nagios-ss3
        ansible-playbook -i inventory/test/inventory site.yml

You might want to run the ansible-playbook twice in order to make it start nagios service automaticlly.

##Use with Packer

You are able to install this Ansible nagios role within an packer instance.

What happens is that packer takes a default virtual machine that have been made in example vmware workstation or your preffered hypervisor.  
You just need to get the virtual machine to be stored in one file in the format of "VMDK" and then you edit the config file for packer where it should grab that virtual machine and the output of the finish product with Nagios installed on it.

**Config - Packer.json**

THIS PACKER.JSON CONFIG IS MADE FOR VMWARE - For more support i suggest checking out [Packer WebSite](https://www.packer.io/docs/builders/index.html)

Tested with packer version: 1.3.3

THe packer.json file is where we configure what packer is going to install inside the virtual machine.

Under the section "builders" you have to change the "source_path" to the location of the virtual machines VMDK from the Hypervisor.

remember to set valid "output_directory"

The section "Provisioners" are all the commands we run inside the virtual machine to get the final product.

``` {
    "builders": [
        {
            "type": "vmware-vmx",
            "source_path": "/home/XXX/vmware/nagios/nagios.vmx",
            "output_directory": "/home/XX/packer/nagios/nagios_build.vmx",
            "ssh_username": "root",
            "ssh_password": "admin123",
            "shutdown_command": "shutdown now -P"
        }

    ],
    "Provisioners": [
        {
            "type": "shell",
            "inline": [
                "apt update -y",
                "apt upgrade -y",
                "apt install git -y",
                "git clone https://gitlab.com/ucl-itt-ss3/nagios-ss3.git",
                "cd nagios-ss3",
                "./runme.sh",
                "./runme.sh"
            ]
        }
    ]
```
When you want to run the the packer config run the command "packer packer.json" then sit back and wait we the complete message.